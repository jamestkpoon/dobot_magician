#!/usr/bin/env python

import rospy
from dobot_magician.srv import *
from dobot_magician.msg import *

import serial, struct  
import numpy as np
from time import sleep
from threading import Thread, Lock

class Dobot_Magician_node:
  def __init__(self): # 'main'
    rospy.init_node('dobot_magician_node', anonymous=True)
    self._pause_dur = 0.05; self._isEq_timeout = 5
    self._isEq_cerr_m = 0.002; self._isEq_jerr_deg = 1.0
    self._state_pub_dur = 0.1; self._iseq = False;
    # serial
    port_ = rospy.get_param('~port'); baud_ = rospy.get_param('~baud')
    try:
      self._ser = serial.Serial(port_,baud_, timeout=1);
      sleep(1.0); serial_ok_ = True
    except:
      serial_ok_ = False
      print '  Failed to connect to Magician at %s. Exiting ...' % port_
    
    if serial_ok_ and self._ser.isOpen():
      # declare internal State msg
      self._state_msg = State(); self._state_mtx = Lock()
      self._state_msg.header.frame_id = '/dobot_magician'
      # Home the robot, deactivate pump
      print '  Deactivating pump and calibrating arm to home position ...'
      self._home_pos = [ rospy.get_param('~hX'), rospy.get_param('~hY'), rospy.get_param('~hZ') ]
      self.goHome(self._home_pos[0], self._home_pos[1], self._home_pos[2])
      #raw_input('  Press Enter when calibration complete (beep + green light) to continue ...')
      print '  This node will now pause for 30 seconds ...'; sleep(30.0)
      # state publish thread
      self._state_pub = rospy.Publisher('/dobot_magician/state', State, queue_size=1)
      suthread_ = Thread(target=self.update_state_loop)
      self._sutrun = True; suthread_.daemon = True; suthread_.start()
      # ROS services
      self._home_srv = rospy.Service('/dobot_magician/home', SetPosCart, self.home_svc)
      self._raw_srv = rospy.Service('/dobot_magician/raw', RawCmd, self.raw_svc)
      self._cart_srv = rospy.Service('/dobot_magician/cart_pos', SetPosCart, self.cart_svc)
      self._jang_srv = rospy.Service('/dobot_magician/joint_angs', SetPosAng, self.jang_svc)
      self._pump_srv = rospy.Service('/dobot_magician/pump', SetPump, self.pump_svc)
      # blocking loop
      print '  Dobot Magician ROS node start!'
      rospy.spin(); self._ser.close()
      self._sutrun = False; suthread_.join(); self._ser.close()

  def getResponse(self): # get latest response starting from ID (hex)
    nbw_ = self._ser.inWaiting(); buf_ = self._ser.read(nbw_); i_ = nbw_-2
    while i_ > 1:
      if buf_[i_-2]=='\xAA' and buf_[i_-1]=='\xAA': return buf_[i_+1:]
      else: i_ = i_ - 1
    return ""
  
  def getState(self):
    # joint angles, end-effector position
    self.sendCommand(2,10,0,1); buf_ = self.getResponse()
    if (len(buf_) > 0) and (ord(buf_[0]) == 10):
      hex_ = buf_[2:34]; pose_arr_ = struct.unpack('%sf'%(len(hex_)/4), hex_)
      self._state_mtx.acquire()
      self._state_msg.header.stamp = rospy.get_rostime()
      self._state_msg.joint_angles = np.deg2rad(pose_arr_[4:]).tolist()
      self._state_msg.pos.x = pose_arr_[0] / 1000
      self._state_msg.pos.y = pose_arr_[1] / 1000
      self._state_msg.pos.z = pose_arr_[2] / 1000
      self._state_mtx.release()
      pos_ok_ = True
    else: pos_ok_ = False
  
  def sendCommand(self, length,ID,rw,isQ,params_str=''): # send serial command
    ctrl_ = (isQ<<4) | rw
    chksum_ = 256 - (ID + ctrl_ + sum(bytearray(params_str)))
    while chksum_ < 0: chksum_ = chksum_ + 256
    cmd_str_ = '\xAA\xAA' + chr(length) \
      + chr(ID) + chr(ctrl_) + params_str + chr(chksum_)
    self._ser.flushOutput(); self._ser.write(cmd_str_)
    sleep(self._pause_dur)
    
  def floats2hstr(self, f): # pack floats to hex string
    f_ = map(float, f)
    return struct.pack('%sf'%len(f_), *f_)

  def setPump(self, desired_pump_status,isQ=0): # vacuum pump control
    self.sendCommand(4,62,1,isQ, chr(1)+chr(desired_pump_status))
    # update pump status
    buf_ = self.getResponse()
    if ord(buf_[0]) == 62:
      self._state_mtx.acquire()
      self._state_msg.pump = ord(buf_[3])
      self._state_mtx.release()

  def isEq_cart(self, x,y,z):
    t_ = 0; t0_ = rospy.get_rostime().to_sec();
    self._iseq = True; isEq_ = False
    while t_<self._isEq_timeout and not isEq_:
      self.getState()
      self._state_mtx.acquire()
      diff_ = [ self._state_msg.pos.x-x, \
        self._state_msg.pos.y-y, \
        self._state_msg.pos.z-z ]
      self._state_mtx.release()
      isEq_ = np.amax(np.fabs(diff_)) <= self._isEq_cerr_m
      t_ = rospy.get_rostime().to_sec() - t0_
    self._iseq = False
    
  def setCart(self, x,y,z,mode=2,isQ=0): # Cartesian control
    cartlist_ = [ x*1000, y*1000, z*1000, 0 ]
    cmd_str_ = chr(mode) + self.floats2hstr(cartlist_)
    self.sendCommand(19,84,1,isQ,cmd_str_); self.isEq_cart(x,y,z)
    
  def isEq_jangs(self, rads):
    t_ = 0; t0_ = rospy.get_rostime().to_sec();
    self._iseq = True; isEq_ = False
    while t_<self._isEq_timeout and not isEq_:
      self.getState()
      self._state_mtx.acquire()
      diff_ = np.asarray(self._state_msg.joint_angles) - np.asarray(rads)
      self._state_mtx.release()
      isEq_ = np.rad2deg(np.amax(np.fabs(diff_))) <= self._isEq_jerr_deg
      t_ = rospy.get_rostime().to_sec() - t0_
    self._iseq = False
    
  def setJangs(self, angles,mode=4,isQ=0): # Joint angle control
    degslist_ = np.rad2deg(angles).tolist()
    cmd_str_ = chr(mode) + self.floats2hstr(degslist_)
    self.sendCommand(19,84,1,isQ,cmd_str_); self.isEq_jangs(angles)
    
  def goHome(self, x,y,z): # deactivate pump, go to Home position
    self.setPump(0,0); sleep(self._pause_dur)
    self.setJangs([0,0.8,0.8,0]); sleep(self._pause_dur)
    home_str_ = self.floats2hstr([x*1000,y*1000,z*1000])
    self.sendCommand(18,30,1,1, home_str_); sleep(self._pause_dur)
    self.sendCommand(3,31,1,1, chr(0))
    
  # ROS related
  def update_state_loop(self): # publish state thread
    self.getState()
    while self._sutrun:
      if not self._iseq: self.getState()
      self._state_mtx.acquire()
      self._state_pub.publish(self._state_msg)
      self._state_mtx.release()
      sleep(self._state_pub_dur)
      
  def home_svc(self, req): # go Home
    if req.pos.x==0.0 and req.pos.y==0.0 and req.pos.z==0.0:
      self.goHome(self._home_pos[0], self._home_pos[1], self._home_pos[2])
    else: self.goHome(req.pos.x, req.pos.y, req.pos.z)
    return []
 
  def raw_svc(self, req): # raw serial commands
    if len(req.cmd) == 2:
       value1 = int(req.cmd[0])
       value2 = int(req.cmd[1])
    self.sendCommand(req.len,req.ID,req.rw,req.isQ,chr(value1) + chr(value2)) #req.cmd
    return RawCmdResponse(res=self.getResponse())
  
  def cart_svc(self, req): # set Cartesian end effector position
    self.setCart(req.pos.x, req.pos.y, req.pos.z); return []
    
  def jang_svc(self, req): # set joint angles
    self.setJangs(req.joint_angles); return []
    
  def pump_svc(self, req): # set pump
    self.setPump(req.pump); return []
    
if __name__ == '__main__':
  dm_node_ = Dobot_Magician_node()
