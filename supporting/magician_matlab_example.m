% Before running this script, please read the Matlab section of the dobot_magician wiki at:
% https://bitbucket.org/jamestkpoon/dobot_magician/wiki/Home

% subscribe to Magician state
sub_ = rossubscriber('/dobot_magician/state');
% receive and print latest state message
disp 'Receiving and printing latest state message ...'
statemsg_ = receive(sub_); showdetails(statemsg_)
fprintf('\n');

pause(0.5)

% move end effector to Cartesian co-ordinate
cartsvc_ = rossvcclient('/dobot_magician/cart_pos');
% create and populate service request (all in metres)
cartmsg_ = rosmessage(cartsvc_);
cartmsg_.Pos.X = 0.2;
cartmsg_.Pos.Y = 0.1;
cartmsg_.Pos.Z = -0.02;
% call
fprintf('Cartesian end-effector control ...\n\n');
cartsvc_.call(cartmsg_);

pause(0.5)

% set joint angles
jangsvc_ = rossvcclient('/dobot_magician/joint_angs');
% create and populate service request (all in radians)
jangmsg_ = rosmessage(jangsvc_);
jangmsg_.JointAngles = [ -0.2, 0.8, 0.6, -0.4 ];
% call
fprintf('Joint angle control ...\n\n');
jangsvc_.call(jangmsg_);

pause(0.5)

% toggle pump for 2 seconds
pumpsvc_ = rossvcclient('/dobot_magician/pump');
pumpmsg_ = rosmessage(pumpsvc_);
fprintf('Pump on ...\n');
pumpmsg_.Pump = 1; pumpsvc_.call(pumpmsg_);
pause(2);
fprintf('Pump off ...\n');
pumpmsg_.Pump = 0; pumpsvc_.call(pumpmsg_);
fprintf('\n');

disp 'Script end'
